var jiraNow = {
  sideButton: function(element) {
    var e = $(element);
    e.addClass("jiraNowSideButton");
    e.attr("data-toggle", "modal");
    e.attr("data-target", "#portfolioModal1");
    var html = '<div class="text-center"><span>Report Issue</span> <i class="far fa-comment-alt fa-2x"></i></div>';
    e.html(html);
  },
};