FROM node:8

# Create app dir
WORKDIR /usr/src/app

# Install app dependencies
COPY package*.json ./

RUN npm set strict-ssl false

RUN npm install

# Bundle app source
COPY . .

EXPOSE 3002
CMD ["npm", "start"]